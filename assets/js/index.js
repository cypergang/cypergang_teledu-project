$(document).ready(function () {
  $(window).resize(function () {
    if (window.innerWidth > 740) {
      $(".visible").toggleClass("visible");
      $(".fas.fa-times-circle").toggleClass("fas fa-times-circle");
      $(".li-click").find("div").toggleClass("visible");
    } else {
    }
  });
  $("li").click(function () {
    $(".li-click").toggleClass("li-click");
    $(".visible").toggleClass("visible");
    $(this).toggleClass("li-click");
    if (!$(".main").hasClass("hidden")) {
      $(".main").addClass("hidden");
    }
    $(this).find($("div")).toggleClass("visible");
    if (window.innerWidth < 740)
      $(".fas.fa-times-circle").toggleClass("fas fa-times-circle");
  });
  $(".fab.fa-accusoft").click(function () {
    if (window.innerWidth < 740) {
      if ($(".fab.fa-accusoft").hasClass("fas fa-times-circle")) {
        $(".list").toggleClass("visible");
        $(this).toggleClass("fas fa-times-circle");
        $(".li-click").find("div").toggleClass("visible");
      } else {
        $(this).toggleClass("fas fa-times-circle");
        $(".visible").toggleClass("visible");
        $(".list").toggleClass("visible");
      }
    } else {
      $(".li-click").toggleClass("li-click");
      $(".visible").toggleClass("visible");
      $(".hidden").toggleClass("hidden");
    }
  });
});
