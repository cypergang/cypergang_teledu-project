$(document).ready(function () {
  $(".fas.fa-bars").click(function () {
    $("#menu").addClass("visible");
  });
  $(".fas.fa-chevron-circle-left").click(function () {
    $("#menu").removeClass("visible");
  });
  $(".infor li").click(function () {
    if ($(this).find("div").hasClass("visible"))
      $(this).find(".visible").removeClass("visible");
    else {
      $(".infor li div").removeClass("visible");
      $(this).find("div").addClass("visible");
    }
  });
  $(window).click(function (event) {
    console.log($(this));
  });
});
